export interface BookingData {
  id: number,
  name: string,
  lastName: string,
  numberOfPeople: number,
  travelClass: string,
  dateOfDeparture: Date,
  dateOfArrival: Date,
  departureAirport: string,
  arrivalAirport: string
}
