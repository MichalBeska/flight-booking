export interface FlightData {
  id: number,
  departureAirport: string,
  arrivalAirport: string,
  dateOfDeparture: Date,
  dateOfArrival: Date
}

export interface AirportData {
  id: number,
  code: string,
  name: string
}
