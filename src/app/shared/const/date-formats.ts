export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'M/d/yy, h:mm a',
  },
  display: {
    dateInput: 'MMM DD, YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};
