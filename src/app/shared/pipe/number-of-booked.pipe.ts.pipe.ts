import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberOfBooked'
})
export class NumberOfBooked implements PipeTransform {

  transform(value: number): string {
    if(value > 9)
        return '9+';
      else if(value > 0)
        return value.toString();
      else
        return '';
  }

}
