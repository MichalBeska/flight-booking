import { Injectable } from "@angular/core";
import { AirportData, FlightData } from "src/app/shared/models/flight.interface";
import { BookingData } from "src/app/shared/models/user.interface";

@Injectable({
    providedIn: "root"
})
export class FlightService {

  public bookedData: BookingData[] = [];

    addBookedFlight(command : BookingData){
      this.bookedData.push(command);
    }

    getBookedFlights(): BookingData[] {
      return this.bookedData;
    }

    getFlights(): FlightData[] {
      const MockData= [
        {id: 1, departureAirport: 'WAW', arrivalAirport: 'LUB', dateOfDeparture:  new Date(), dateOfArrival: new Date()},
        {id: 2, departureAirport: 'WAW', arrivalAirport: 'HAM', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 3, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 4, departureAirport: 'WAW', arrivalAirport: 'HAM', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 5, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 6, departureAirport: 'WAW', arrivalAirport: 'HAM', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 7, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 8, departureAirport: 'WAW', arrivalAirport: 'HAM', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 9, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 10, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
        {id: 11, departureAirport: 'LUB', arrivalAirport: 'OSL', dateOfDeparture: new Date(), dateOfArrival: new Date()},
      ];
      return MockData;
    }

    getAirport(): AirportData[]{
      const MockData= [
        {id: 1, code: 'WAW', name: 'Warszawa Okęcie'},
        {id: 2, code: 'LUB', name: 'Lublin Świdnik'},
        {id: 1, code: 'HAM', name: 'Hamburg Airport'},
        {id: 1, code: 'OSL', name: 'Oslo Airport'},
      ];
      return MockData;
    }
  }
