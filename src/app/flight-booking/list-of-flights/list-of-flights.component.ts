import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FlightData } from 'src/app/shared/models/flight.interface';

@Component({
  selector: 'app-list-of-flights',
  templateUrl: './list-of-flights.component.html',
  styleUrls: ['./list-of-flights.component.scss']
})
export class ListOfFlightsComponent{

  @Input() listData: FlightData[];

  @Output() flightData = new EventEmitter<FlightData>();

  public displayedColumns: string[] = ['flightCode', 'dateOfDeparture', 'button'];
  public dataSource: FlightData[];
  public clickedRow: FlightData;

  onClickRow(value : FlightData) {
    this.clickedRow = value;
    this.flightData.emit(value);
  }
}
