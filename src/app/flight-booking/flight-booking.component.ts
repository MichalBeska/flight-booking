import { Component, OnInit } from '@angular/core';
import { FlightService } from '../core/services/flight.service';
import { AirportData, FlightData } from '../shared/models/flight.interface';
import { BookingData } from '../shared/models/user.interface';



@Component({
  selector: 'app-flight-booking',
  templateUrl: './flight-booking.component.html',
  styleUrls: ['./flight-booking.component.scss']
})
export class FlightBookingComponent implements OnInit {

  public bookedFlights: BookingData[] = [];
  public dataSource: FlightData[];
  public airports: AirportData[];
  public flightData: FlightData;

  constructor(private readonly _flightService: FlightService
  ) { }

  ngOnInit(): void {
    this.dataSource = this._flightService.getFlights();
    this.airports = this._flightService.getAirport();
  }

  changedListOfFlights(value : FlightData) {
    this.flightData = value
  }

  changedBooking(value : BookingData) {
    this._flightService.addBookedFlight(value);
    this.bookedFlights = this._flightService.getBookedFlights();
  }
}
