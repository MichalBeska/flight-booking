import { Component, Input} from '@angular/core';
import { BookingData } from 'src/app/shared/models/user.interface';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent{

  @Input() bookedFlights: BookingData[];
  
}
