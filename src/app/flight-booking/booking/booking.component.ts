import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AirportData, FlightData } from 'src/app/shared/models/flight.interface';
import { BookingData } from 'src/app/shared/models/user.interface';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  @Input() airports: AirportData[];
  @Input() flightData: FlightData;

  @Output() bookingDataOutput = new EventEmitter<BookingData>();

  flightBooking = this._fb.group({
    name:["",[Validators.required]],
    lastName:["",[Validators.required]],
    numberOfPeople:["",[Validators.required]],
    travelClass:["",[Validators.required]],
  })

  travelClasses: string[] = [
    'Ekonominczna',
    'Biznesowa',
    'Pierwsza'
  ];

  public date: number;
  public name: string = '';
  public lastName: string = '';
  public numberOfPeople: number;
  public travelClass: string = '';
  public bookingData: BookingData = {
    id: 0,
    name: '',
    lastName: '',
    numberOfPeople: 0,
    travelClass: '',
    dateOfDeparture: new Date(),
    dateOfArrival: new Date(),
    departureAirport: '',
    arrivalAirport: ''
  };

  constructor(private _fb: FormBuilder
    ) { }

  ngOnInit(): void {
    this.date = Date.now()
  }

  submit() {
    this.bookingData.name = this.flightBooking.value.name;
    this.bookingData.lastName = this.flightBooking.value.lastName;
    this.bookingData.numberOfPeople = this.flightBooking.value.numberOfPeople;
    this.bookingData.travelClass = this.flightBooking.value.travelClass;
    this.bookingData.dateOfDeparture = this.flightData.dateOfDeparture;
    this.bookingData.dateOfArrival = this.flightData.dateOfArrival;
    this.bookingData.departureAirport = this.flightData.departureAirport;
    this.bookingData.arrivalAirport = this.flightData.arrivalAirport;

    this.bookingDataOutput.emit({...this.bookingData});
  }
}
