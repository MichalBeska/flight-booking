import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ToolbarComponent } from './flight-booking/toolbar/toolbar.component';
import { FlightBookingComponent } from './flight-booking/flight-booking.component';
import { MatTableModule } from '@angular/material/table';
import { FooterComponent } from './flight-booking/footer/footer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from './shared/const/date-formats';
import { ListOfFlightsComponent } from './flight-booking/list-of-flights/list-of-flights.component';
import { BookingComponent } from './flight-booking/booking/booking.component';
import { NumberOfBooked } from './shared/pipe/number-of-booked.pipe.ts.pipe'
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    FlightBookingComponent,
    FooterComponent,
    ListOfFlightsComponent,
    BookingComponent,
    NumberOfBooked
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormsModule,
    MatBadgeModule,
    MatMenuModule
  ],
  providers: [
    { provide: MAT_DATE_FORMATS,
      useValue: MY_DATE_FORMATS,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
